<?php
namespace App\Booking\Presenters;

trait CommentPresenter {

    public function getRatingAttribute($value)
    {
        $str = '';

        for($i=1; $i<=5; $i++)
        {
            $negr = '';

            if($value == 1 && $i > 1)
            $negr = 'negatice-rating';

            elseif($value == 2 && $i > 2)
            $negr = 'negatice-rating';

            elseif($value == 3 && $i > 3)
            $negr = 'negatice-rating';

            elseif($value == 4 && $i > 4)
            $negr = 'negatice-rating';

            $str .= '<span class="glyphicon glyphicon-star '.$negr.'" aria-hidden="true"></span>';
        }
        
        return $str;
    }
}