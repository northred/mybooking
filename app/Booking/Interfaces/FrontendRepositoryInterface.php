<?php

namespace App\Booking\Interfaces;

interface FrontendRepositoryInterface{
    
    public function getObjectsForMainPage();

    public function getObject($id);
}