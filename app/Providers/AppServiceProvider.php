<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\App;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if(App::environment('local'))
        {
            $this->app->bind(\App\Booking\Interfaces\FrontendRepositoryInterface::class,function()
            {
                return new \App\Booking\Repositories\FrontendRepository;
            });
        }
        else
        {
            $this->app->bind(\App\Booking\Interfaces\FrontendRepositoryInterface::class,function()
            {
                return new \App\Booking\Repositories\CashedFrontendRepository;
            });
        }
        

        $this->app->bind(\App\Booking\Interfaces\BackendRepositoryInterface::class, function()
        {
            return new \App\Booking\Repositories\BackendRepository;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('backend.*', '\App\Booking\ViewComposers\BackendComposer');

        View::composer('frontend.*', function($view){
            $view->with('placeholder', asset('images/placeholder.png'));
        });

        if(App::environment('local'))
        {
            View::composer('*', function($view){
                $view->with('novalidate', 'novalidate');
            });
        }
        else
        {
            View::composer('*', function($view){
                $view->with('novalidate', null);
            });
        }
    }
}
